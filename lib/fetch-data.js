const sleep = (ms) => new Promise((r) => setTimeout(r, ms));

export default async function fetchData(type, delay = 0) {
  console.log(`fetchData called: type: ${type} with delay ${delay}`);
  await sleep(delay);
  const [res] = await Promise.all([
    fetch(`https://hacker-news.firebaseio.com/v0/${type}.json`)
  ])
  if (res.status !== 200) {
    throw new Error(`Status ${res.status}`)
  }
  return res.json()
}