const cache = {}

export default function useData(key, fetcher) {
  console.log(`useData called with key: ${key}`);
  if (!cache[key]) {
    let data
    let error
    let promise
    cache[key] = () => {
      if (error !== undefined || data !== undefined) {
        return { data, error }
      }

      if (!promise) {
        promise = fetcher()
        .then((r) => (data = r))
        // Convert all errors to plain string for serialization
        .catch((e) => error = e + '')
      } 
      throw promise
    }
  }

  return cache[key]()
}

export function clearCache(key) {
  console.log(`Clearing cache for key: ${key}`);
  if (cache[key]) {
    delete cache[key];
  }
}