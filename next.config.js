module.exports = {
  reactStrictMode: false,
  experimental: {
    runtime: 'experimental-edge',
    serverComponents: true,
  },
}
